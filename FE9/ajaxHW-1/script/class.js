class API {
    constructor() {
        this.url = 'https://swapi.co/api/films/';
    }

    getByFetch() {
        return fetch(this.url)
            .then(data => data.json())
            .then(dataParse => dataParse.results)
    }
}

class Films extends API {
    getFilmsInfo() {
        return super.getByFetch()
            .then(data => data.map
            (film => {
                return ul.innerHTML += `<li>
                                          <ul><li class="filmTitle">${film.title}</li>
                                          <li class="filmepisode_id">${film.episode_id}</li>
                                          <li class="filmopening_crawl">${film.opening_crawl}</li></ul>
                                       </li>`
            }))

    }
}

class Characters extends Films {
    getCharactersInfo() {
        return super.getByFetch()
            .then(data => data.forEach(
            (film =>
            {
                const s = document.getElementsByClassName('filmTitle');
                for (let title of s){
                   film.characters.map(item => fetch(item)
                       .then(items => items.json())
                       .then(item => title.innerHTML +=`<ul><li>${item.name}</li></ul>`))
                }
            }
            )

            ))
    }
}


const x = new Films();
console.log(x.getFilmsInfo());

const c = new Characters();
console.log(c.getCharactersInfo());
