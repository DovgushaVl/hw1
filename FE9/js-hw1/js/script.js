/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */

function Hamburger(size, stuffing) {
    this.size = size;
    this.stuffing = stuffing;
    this.topping = [];

}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {
    price: 50,
    cal: 20
};
Hamburger.SIZE_LARGE = {
    price: 100,
    cal: 40
};
Hamburger.STUFFING_CHEESE = {
    price: 20,
    cal: 20
};
Hamburger.STUFFING_SALAD = {
    price: 20,
    cal: 5
};
Hamburger.STUFFING_POTATO = {
    price: 15,
    cal: 10
};
Hamburger.TOPPING_MAYO = {
    price: 20,
    cal: 5
};
Hamburger.TOPPING_SPICE = {
    price: 15,
    cal: 0
};

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 *
 */

Hamburger.prototype.addTopping = function (topping) {
    var vot = false;
    if (this.topping.length < 1) {
        this.topping.push(topping);
    } else {
        for (let top of  this.topping) {
            if (JSON.stringify(top) === JSON.stringify(topping)) {
                vot = true;
            }
        }
        if (!vot) {
            this.topping.push(topping);
        }
    }
};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    var vot = false;
    if (this.topping.length === 0){
        alert('niXera')
    }

     else {
        for (let top of  this.topping) {
            if (JSON.stringify(top) === JSON.stringify(topping)) {
                vot = true;
            }
        }
        if (vot) {
            this.topping.splice(topping,1);
        }
    }
};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
};

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException() {
}


// console.dir(hamburger.size);

var hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);