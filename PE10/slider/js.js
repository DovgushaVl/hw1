function clickBtn() {
    let btns = document.getElementsByClassName('menuInsetBtn');
    let divs = document.getElementsByClassName('btnText');
    for (let i =0; i <btns.length; i++)
        btns[i].onclick = function () {
            for (let btn of btns) {
                btn.classList.remove('active');
            }

            btns[i].classList.add('active');
            let div = divs[i];
            for (let div of divs) {
                div.classList.remove('active');
            }
            div.classList.add('active');
        }

}
clickBtn();