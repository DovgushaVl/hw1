document.addEventListener('DOMContentLoaded', onReady);

function onReady() {
    input.addEventListener('focus', function () {
        console.log(document.getElementsByTagName('label'));
        label.innerHTML = '';
        // document.body.removeChild(label);
        input.style.border = '2px solid green';
        input.style.borderRadius = '11px';
        input.style.outline = 'none';
    });
    input.addEventListener('blur', check);

    function check() {
        if (input.value > 0) {
            return ok();
        } else {
            return error();
        }
    }


    const ok = function () {
        input.style.border = '1px solid black';
        input.style.borderRadius = '11px';
        input.style.outline = 'none';
        input.style.color = 'green';
        if (!document.querySelector('span') && !document.querySelector('button')) {

            span.style.display = 'inline-block';
            form.insertBefore(span, div);
            span.innerHTML = `Текущая цена: $${input.value}`;


            form.insertBefore(btn, div);
            btn.style.display = 'inline-block';
            btn.style.marginLeft = '40px';
            btn.innerHTML = `X`;
        } else {
            span.innerHTML = `Текущая цена: $${input.value}`;
        }
    };
    const error = function () {
        input.style.border = '2px solid red';
        input.style.borderRadius = '11px';
        input.style.color = 'red';
        if (document.querySelector('span')) {
            form.removeChild(span);
            form.removeChild(btn);
        }
        document.body.appendChild(label);
        label.innerHTML = `Please enter correct price`;
    };
}
const inputs = document.getElementsByTagName('input');
const input = inputs[0];
const form = document.querySelector('form');
const div = document.querySelector('div');
const span = document.createElement('span');
const btn = document.createElement('button');
const label = document.createElement('label');