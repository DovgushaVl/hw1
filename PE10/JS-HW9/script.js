document.addEventListener('DOMContentLoaded', onReady);

function onReady() {
    const menus = document.getElementsByClassName('tabs-title');
    const cont = document.getElementsByClassName('item');


    for (let menu of menus) {
        menu.classList.remove('active');


        menu.onclick = function () {

                // menu.classList.remove('active');

            menu.classList.add('active');
            menu.style.background = 'red';
            let x = cont[menu.id - 1];
            for (let i of cont) {
                if (i.classList.contains('active')) {
                    i.classList.remove('active')
                }
            }
            x.classList.add("active");
        }

    }
}
