function clickBtn() {
    let btns = document.getElementsByClassName('menuInsetBtn');
    let divs = document.getElementsByClassName('btnText');
    for (let i = 0; i < btns.length; i++)
        btns[i].onclick = function () {
            for (let btn of btns) {
                btn.classList.remove('active');
            }

            btns[i].classList.add('active');
            let div = divs[i];
            for (let div of divs) {
                div.classList.remove('active');
            }
            div.classList.add('active');
        }

}

$('.amWkMenuBtn').click(function () {
    $('.amWkMenuBtn').removeClass('activeItem');
    $(this).addClass('activeItem');
    let btnTitle = $(this).attr('title');
    $('img').hide();
    if (btnTitle === 'all') {
        $('img').show();
    }
    $('img.' + btnTitle).show();
});
$('.awBtn').click(function () {
    $('.awBtn').css('display', 'none');
    $('.cssload-thecube').show();
    setTimeout(function () {
        $('.cssload-thecube').hide();
        $('.sec12').css('display', 'flex');
    }, 3000);
});


$('.prev').click(function () {
    if ($('.activeSldImg').index() <= 1) {
        $('.activeSldImg').removeClass('activeSldImg');
        $($('.bordShad2')[3]).addClass('activeSldImg');

        $('.firstPrs.WsActive').removeClass('WsActive');
        $($('.firstPrs')[3]).addClass('WsActive');

    } else {
        $('.activeSldImg').removeClass('activeSldImg').prev().addClass('activeSldImg');
        $('.firstPrs.WsActive').removeClass('WsActive').prev().addClass('WsActive');
    }

});
$('.next').click(function () {
    if ($('.activeSldImg').index() >= 4) {
        $('.activeSldImg').removeClass('activeSldImg');
        $($('.bordShad2')[0]).addClass('activeSldImg');

        $('.firstPrs.WsActive').removeClass('WsActive');
        $($('.firstPrs')[0]).addClass('WsActive');

    } else {
        $('.activeSldImg').removeClass('activeSldImg').next().addClass('activeSldImg');
        $('.firstPrs.WsActive').removeClass('WsActive').next().addClass('WsActive');
    }
});
$('.bordShad2').click(function () {
    $('.bordShad2').removeClass('activeSldImg');
    $(this).addClass('activeSldImg');
    const indx = $(this).index();

    $('.firstPrs').removeClass('WsActive')
    $($('.firstPrs')[indx - 1]).addClass('WsActive')
});
clickBtn();




